# double-SVM

This folder containts everything related to the publication of the paper entitled: "Non-small cell lung cancer survival estimation through double-SVM: a multi-omics and multi-sources integrative model".

## Getting started

The following R Packages have to be installed for proper execution of the scripts:
- pandas
- numpy
- getpass
- os
- io
- random
- warnings
- sklearn
- sksurv
- seaborn

If the loading/installation of these packages shows some problems, the code will not run.

## Data

The "data" folder contains the four TCGA matrices that have been use in the paper for training and cross-validation of the model (i.e. clinical variables, gene expression, copy number and mutations).

## Files

The directory contains the following files:
- doubleSVM.ipynb a Jupyter notebook with the implementation of the double_svm and exemplary usage. In particular, the script implements the process of cross-validation on TCGA data and exemplary boxplot of the results.

## Usage

Run the notebook.

## Authors and acknowledgment

Work has been performed by aizoOn Technology consulting as part of project DEFLeCT (Digital tEChnology For Lung Cancer Treatment).

<img src="https://aizoongroup.com/Style%20Library/images/logo.png" width="350">

## License

This work is licensed under the Gnu Affero Public License V3.0 (see https://gitlab.com/deflect-public/consensus-clustering/-/blob/main/LICENSE)

<img src="https://www.regione.piemonte.it/loghi/im/grafica/piede_fesr.jpg">
